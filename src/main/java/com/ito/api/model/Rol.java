package com.ito.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "JUAN_PABLO_RR_ROL")
public class Rol {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ROL_SEQ")
    @SequenceGenerator(sequenceName = "rol_seq", allocationSize = 1, name = "ROL_SEC")
	private Integer id;

	@Column(name = "NOMBRE", length = 100, nullable = false, unique = true)
	private String nombre;

	@Column(name = "DESCRIPCION", length = 300, nullable = true)
	private String descripcion;
	
	@Column(name = "ACTIVO", length = 1, nullable = false)
	private Integer activo;
	
	@Column(name = "ELIMINADO", length = 1, nullable = false)
	private Integer eliminado;
	
	public Rol() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	public Integer getEliminado() {
		return eliminado;
	}

	public void setEliminado(Integer eliminado) {
		this.eliminado = eliminado;
	}
	
}
