package com.ito.api.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "JUAN_PABLO_RR_MENSAJE")
public class Mensaje {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MENSAJE_SEQ")
    @SequenceGenerator(sequenceName = "mensaje_seq", allocationSize = 1, name = "MENSAJE_SEC")
	private Integer id;

	@Column(name = "LEIDO", length = 1, nullable = false)
	private Integer leido;

	@Column(name = "FECHA_REGISTRO", nullable = false)
	private Date fechaRegistro;

	@Column(name = "ORIGEN", length = 50, nullable = false)
	private String origen;

	@Column(name = "MENSAJE", length = 300, nullable = false)
	private String mensaje;
	
	@Column(name = "ACTIVO", length = 1, nullable = false)
	private Integer activo;
	
	@Column(name = "ELIMINADO", length = 1, nullable = false)
	private Integer eliminado;
	
	public Mensaje() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getLeido() {
		return leido;
	}

	public void setLeido(Integer leido) {
		this.leido = leido;
	}

	public Date getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(Date fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	public Integer getEliminado() {
		return eliminado;
	}

	public void setEliminado(Integer eliminado) {
		this.eliminado = eliminado;
	}
	
	
}
