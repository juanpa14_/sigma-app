package com.ito.api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "JUAN_PABLO_RR_USUARIO")
public class Usuario {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USUARIO_SEQ")
    @SequenceGenerator(sequenceName = "usuario_seq", allocationSize = 1, name = "USUARIO_SEC")
	private Integer id;

	@Column(name = "USUARIO", length = 50, nullable = false, unique = true)
	private String usuario;

	@Column(name = "CLAVE", length = 255, nullable = true)
	private String clave;

	@Column(name = "IDENTIFICACION", length = 20, nullable = false)
	private String identificacion;

	@Column(name = "APELLIDOS", length = 255, nullable = false)
	private String apellidos;

	@Column(name = "NOMBRES", length = 255, nullable = false)
	private String nombres;

	@Column(name = "ORIGEN_ID", nullable = true)
	private Integer origenId;

	@Column(name = "CORREO_ELECTRONICO", length = 255, nullable = true)
	private String correoElectronico;

	@Column(name = "INTENTOS_FALLIDOS", nullable = false)
	private Integer intentosFallidos;

	@Column(name = "INTENTOS_EXITOSOS", nullable = false)
	private Integer intentosExitosos;
	
	@Column(name = "ACTIVO", length = 1, nullable = false)
	private Integer activo;
	
	@Column(name = "ELIMINADO", length = 1, nullable = false)
	private Integer eliminado;
	
	public Usuario() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public Integer getOrigenId() {
		return origenId;
	}

	public void setOrigenId(Integer origenId) {
		this.origenId = origenId;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public Integer getIntentosFallidos() {
		return intentosFallidos;
	}

	public void setIntentosFallidos(Integer intentosFallidos) {
		this.intentosFallidos = intentosFallidos;
	}

	public Integer getIntentosExitosos() {
		return intentosExitosos;
	}

	public void setIntentosExitosos(Integer intentosExitosos) {
		this.intentosExitosos = intentosExitosos;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	public Integer getEliminado() {
		return eliminado;
	}

	public void setEliminado(Integer eliminado) {
		this.eliminado = eliminado;
	}

}
