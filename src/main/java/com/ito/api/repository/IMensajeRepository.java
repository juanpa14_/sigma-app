package com.ito.api.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ito.api.model.Mensaje;

@Repository
public interface IMensajeRepository extends CrudRepository<Mensaje, Integer>{

}
