package com.ito.api.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ito.api.model.Rol;

@Repository
public interface IRolRepository extends CrudRepository<Rol, Integer>{

	List<Rol> findByNombre(String nombre);
}
