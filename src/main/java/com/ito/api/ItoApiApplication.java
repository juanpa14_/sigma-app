package com.ito.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ItoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ItoApiApplication.class, args);
	}

}
