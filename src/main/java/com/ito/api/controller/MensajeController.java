package com.ito.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ito.api.dto.MensajeDto;
import com.ito.api.model.Mensaje;
import com.ito.api.service.MensajeService;

@RestController
@RequestMapping(path = "/mensaje")
public class MensajeController {

	@Autowired
	private MensajeService mensajeService;

	@GetMapping(path = "/list")
	public List<Mensaje> getList() {
		return this.mensajeService.findAll();
	}
	
	@GetMapping(path = "/{id}")
	public Mensaje getMensajeById(@PathVariable Integer id) {
		return this.mensajeService.getById(id);
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<?> deleteMensaje(@PathVariable Integer id) {
		try {
			this.mensajeService.deleteMensaje(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping(path = "/{id}")
	public ResponseEntity<?> updateMensaje(@RequestBody MensajeDto request, @PathVariable Integer id) {
		try {
			this.mensajeService.updateMensaje(this.buildMensajeFromRequest(request), id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path = "/")
	public ResponseEntity<?> insertMensaje(@RequestBody MensajeDto request) {
		try {
			this.mensajeService.insertMensaje(this.buildMensajeFromRequest(request));
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	private Mensaje buildMensajeFromRequest(MensajeDto request) {
		
		Mensaje mensaje = new Mensaje();
		
		mensaje.setActivo(request.getActivo());
		mensaje.setEliminado(request.getEliminado());
		mensaje.setFechaRegistro(request.getFechaRegistro());
		mensaje.setId(request.getId());
		mensaje.setLeido(request.getLeido());
		mensaje.setMensaje(request.getMensaje());
		mensaje.setOrigen(request.getOrigen());
		
		return mensaje;
	}
	
}
