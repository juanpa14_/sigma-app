package com.ito.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ito.api.dto.RolDto;
import com.ito.api.model.Rol;
import com.ito.api.service.RolService;

@RestController
@RequestMapping(path = "/rol")
public class RolController {
	
	@Autowired
	private RolService rolService;

	@GetMapping(path = "/list")
	public List<Rol> getList() {
		return this.rolService.findAll();
	}
	
	@GetMapping(path = "/{id}")
	public Rol getRolById(@PathVariable Integer id) {
		return this.rolService.getById(id);
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<?> deleteRol(@PathVariable Integer id) {
		try {
			this.rolService.deleteRol(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping(path = "/{id}")
	public ResponseEntity<?> updateRol(@RequestBody RolDto request, @PathVariable Integer id) {
		try {
			this.rolService.updateRol(this.buildMensajeFromRequest(request), id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path = "/")
	public ResponseEntity<?> insertRol(@RequestBody RolDto request) {
		try {
			this.rolService.insertRol(this.buildMensajeFromRequest(request));
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	private Rol buildMensajeFromRequest(RolDto request) {
		
		Rol rol = new Rol();
		
		rol.setActivo(request.getActivo());
		rol.setDescripcion(request.getDescripcion());
		rol.setEliminado(request.getEliminado());
		rol.setId(request.getId());
		rol.setNombre(request.getNombre());
		
		return rol;
	}
	
}
