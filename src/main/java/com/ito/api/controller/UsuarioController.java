package com.ito.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ito.api.dto.UsuarioDto;
import com.ito.api.model.Usuario;
import com.ito.api.service.UsuarioService;

@RestController
@RequestMapping(path = "/usuario")
public class UsuarioController {
	
	@Autowired
	private UsuarioService usuarioService;

	@GetMapping(path = "/list")
	public List<Usuario> getList() {
		return this.usuarioService.findAll();
	}
	
	@GetMapping(path = "/{id}")
	public Usuario getUserById(@PathVariable Integer id) {
		return this.usuarioService.getById(id);
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable Integer id) {
		try {
			this.usuarioService.deleteUser(id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping(path = "/{id}")
	public ResponseEntity<?> updateUser(@RequestBody UsuarioDto request, @PathVariable Integer id) {
		try {
			this.usuarioService.updateUser(this.buildUsuarioFromRequest(request), id);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(path = "/")
	public ResponseEntity<?> insertUser(@RequestBody UsuarioDto request) {
		try {
			this.usuarioService.insertUser(this.buildUsuarioFromRequest(request));
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	private Usuario buildUsuarioFromRequest(UsuarioDto request) {
		
		Usuario usuario = new Usuario();
		
		usuario.setActivo(request.getActivo());
		usuario.setApellidos(request.getApellidos());
		usuario.setClave(request.getClave());
		usuario.setCorreoElectronico(request.getCorreoElectronico());
		usuario.setEliminado(request.getEliminado());
		usuario.setId(request.getId());
		usuario.setIdentificacion(request.getIdentificacion());
		usuario.setIntentosExitosos(request.getIntentosFallidos());
		usuario.setIntentosFallidos(request.getIntentosFallidos());
		usuario.setNombres(request.getNombres());
		usuario.setOrigenId(request.getOrigenId());
		usuario.setUsuario(request.getUsuario());
		
		return usuario;
	}

}
