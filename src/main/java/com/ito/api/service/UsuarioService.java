package com.ito.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ito.api.model.Usuario;
import com.ito.api.repository.IUsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private IUsuarioRepository usuarioRepository;


	public List<Usuario> findAll() {
		// Consulta los usuarios en la capa de datos
		List<Usuario> list = new ArrayList<Usuario>();
		this.usuarioRepository.findAll().forEach(e -> list.add(e));
		
		return list;
	}
	
	public Usuario getById(Integer id) {
		// Consulta el usuario en la capa de datos
		Optional<Usuario> data = usuarioRepository.findById(id);
		
		return data != null && data.isPresent() ? data.get() : null;	
	}
	
	public void deleteUser(Integer id) throws Exception {
		// Elimina el usuario si existe
		if (this.usuarioRepository.existsById(id)) {
			throw new Exception("Usuario no existe");
		}
		
		this.usuarioRepository.deleteById(id);
	}
	
	public void updateUser(Usuario usuario, Integer id) throws Exception {
		
		Usuario usuarioConsulta = this.getById(id);
		
		if (usuarioConsulta == null) {
			throw new Exception("Usuario no existe");
		}
		
		List<Usuario> data = this.usuarioRepository.findByUsuario(usuario.getUsuario());
		
		if (data != null && !data.isEmpty()) {
			throw new Exception("Existe un usuario con el nombre ingresado");
		}
		
		this.usuarioRepository.save(usuarioConsulta);
	}
	
	public void insertUser(Usuario usuario) throws Exception {
		List<Usuario> data = this.usuarioRepository.findByUsuario(usuario.getUsuario());
		
		if (data != null && !data.isEmpty()) {
			throw new Exception("Existe un usuario con el nombre ingresado");
		}
		
		this.usuarioRepository.save(usuario);
	}
	
}
