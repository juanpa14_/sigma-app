package com.ito.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ito.api.model.Mensaje;
import com.ito.api.repository.IMensajeRepository;

@Service
public class MensajeService {

	@Autowired
	private IMensajeRepository mensajeRepository;


	public List<Mensaje> findAll() {
		// Consulta los mensajes en la capa de datos
		List<Mensaje> list = new ArrayList<Mensaje>();
		this.mensajeRepository.findAll().forEach(e -> list.add(e));
		
		return list;
	}
	
	public Mensaje getById(Integer id) {
		// Consulta el mensaje en la capa de datos
		Optional<Mensaje> data = mensajeRepository.findById(id);
		
		return data != null && data.isPresent() ? data.get() : null;	
	}
	
	public void deleteMensaje(Integer id) throws Exception {
		// Elimina elensaje si existe
		if (!this.mensajeRepository.existsById(id)) {
			throw new Exception("Mensaje no existe");
		}
		
		this.mensajeRepository.deleteById(id);
	}
	
	public void updateMensaje(Mensaje mensaje, Integer id)  throws Exception {
		
		Mensaje mensajeConsulta = this.getById(id);
		
		if (mensajeConsulta == null) {
			throw new Exception("Mensaje no existe");
		}
		
		this.mensajeRepository.save(mensajeConsulta);
	}
	
	public void insertMensaje(Mensaje mensaje) {
		this.mensajeRepository.save(mensaje);
	}
	
}
