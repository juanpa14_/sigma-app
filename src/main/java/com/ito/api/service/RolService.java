package com.ito.api.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ito.api.model.Rol;
import com.ito.api.repository.IRolRepository;

@Service
public class RolService {

	@Autowired
	private IRolRepository rolRepository;


	public List<Rol> findAll() {
		// Consulta los roles en la capa de datos
		List<Rol> list = new ArrayList<Rol>();
		this.rolRepository.findAll().forEach(e -> list.add(e));
		
		return list;
	}
	
	public Rol getById(Integer id) {
		// Consulta el rol en la capa de datos
		Optional<Rol> data = rolRepository.findById(id);
		
		return data != null && data.isPresent() ? data.get() : null;	
	}
	
	public void deleteRol(Integer id) {
		// Elimina el rol si existe
		if (!this.rolRepository.existsById(id)) {
			
		}
	
		this.rolRepository.deleteById(id);
	}
	
	public void updateRol(Rol rol, Integer id) {
		
		Rol rolConsulta = this.getById(id);
		
		if (rolConsulta == null) {
			
		}
		
		List<Rol> data = this.rolRepository.findByNombre("nombre");
		
		if (data == null || data.isEmpty()) {
			
		}
		
		this.rolRepository.save(rolConsulta);
	}
	
	public void insertRol(Rol rol) {
		List<Rol> data = this.rolRepository.findByNombre("nombre");
		
		if (data == null || data.isEmpty())
			this.rolRepository.save(rol);
	}
	
}
