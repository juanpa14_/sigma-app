package com.ito.api.dto;

public class UsuarioDto {

	private Integer id;
	private String usuario;
	private String clave;
	private String identificacion;
	private String apellidos;
	private String nombres;
	private Integer origenId;
	private String correoElectronico;
	private Integer intentosFallidos;
	private Integer intentosExitosos;
	private Integer activo;
	private Integer eliminado;
	
	public UsuarioDto() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public Integer getOrigenId() {
		return origenId;
	}

	public void setOrigenId(Integer origenId) {
		this.origenId = origenId;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public Integer getIntentosFallidos() {
		return intentosFallidos;
	}

	public void setIntentosFallidos(Integer intentosFallidos) {
		this.intentosFallidos = intentosFallidos;
	}

	public Integer getIntentosExitosos() {
		return intentosExitosos;
	}

	public void setIntentosExitosos(Integer intentosExitosos) {
		this.intentosExitosos = intentosExitosos;
	}

	public Integer getActivo() {
		return activo;
	}

	public void setActivo(Integer activo) {
		this.activo = activo;
	}

	public Integer getEliminado() {
		return eliminado;
	}

	public void setEliminado(Integer eliminado) {
		this.eliminado = eliminado;
	}

}
